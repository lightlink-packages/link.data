﻿using Link.Data.Models;
using System.Threading.Tasks;

namespace Link.Data.Repositories.Interfaces
{
    public interface ILinkRepositiry
    {
        Task<CompressLink> AddLinkAsync(CompressLink link);
        Task<CompressLink> GetLinkByCompressedLinkAsync(string compressedLink);
    }
}