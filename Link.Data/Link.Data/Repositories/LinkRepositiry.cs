﻿using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;
using Link.Data.Models;
using Link.Data.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Link.Data.Repositories
{
    public class LinkRepositiry : ILinkRepositiry
    {
        public async Task<CompressLink> AddLinkAsync(CompressLink link)
        {
            using (var client = new AmazonDynamoDBClient())
            using (var context = new DynamoDBContext(client))
            {
                await context.SaveAsync(link);

                return link;
            }
        }

        public async Task<CompressLink> GetLinkByCompressedLinkAsync(string compressedLink)
        {
            using (var client = new AmazonDynamoDBClient())
            using (var context = new DynamoDBContext(client))
            {
                var search = context.ScanAsync<CompressLink>(new List<ScanCondition>()
                {
                    new ScanCondition("CompressedLink", ScanOperator.Equal, compressedLink)
                }, new DynamoDBOperationConfig()
                {
                    IndexName = "DecompressedLink"
                });

                var links = await search.GetRemainingAsync();

                return links.FirstOrDefault();

                // OR

                //var link = await context.LoadAsync<CompressLink>(compressedLink, new DynamoDBOperationConfig()
                //{
                //    IndexName = "DecompressedLink"
                //});

                //return link;
            }
        }
    }
}
